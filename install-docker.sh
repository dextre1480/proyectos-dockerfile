#!/bin/bash

setenforce 0
systemctl stop firewalld
echo 's' | yum install epel-release -y

yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine

echo 's' | yum install yum-utils unzip zip vim -y

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
echo 's' | yum update -y
yum install docker-ce -y

####instalar docker-compose
yum install curl -y
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
echo 'y' | yum install docker-compose -y

systemctl enable --now docker containerd


systemctl start docker

systemctl status docker

systemctl enable docker



