#!/bin/bash

#while :
#do
#echo "Escoga una opcion "
#echo "1.- DESHABILITA SELINUX, COMENTAR CP, ACTUALIZACION Y REBOOT.  "
#echo "2.- INSTALACION DE COMPLEMENTOS y REBOOT.  "
#echo "3.- INSTALACION DE: 
#          DAHDI LINUX.
#          DAHDA TOOLS.
#          LIBPRI.
#          MPG123.
#          SPANDSP.
#          PHP.
#          JANSSON.
#          ASTERISK 13.32. 
#          SONIDOS.
#          MARIADB. "
#echo "0.- cero para salir. "  
#echo -n "Seleccione una opcion del 1 al 3, o CERO para salir,
#SU OPCION ELEGIDA ES => "
#read opcion
#case $opcion in
#
#1) echo "DESAHABILITANDO SELINUX, COMENTAR CP Y ACTUALIZACION DEL SISTEMA "
#
echo "#####################"
echo "## CENTOS 7 #########"
echo "#####################"

setenforce 0
sed -i "1,7s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config
sed -i "6s/alias cp='cp -i'/#alias cp='cp -i'/" /root/.bashrc
sed -i "7s/alias mv='mv -i'/#alias mv='mv -i'/" /root/.bashrc
systemctl stop firewalld
systemctl disable firewalld

####yum update -y

echo "#############################"
echo "### haremos un reinicio ######"
echo "#############################"

#reboot

#;;


#2) echo "INSTALACION DE COMPLEMENTOS y REINICIO DEL SISTEMA "

echo "###############################################################"
echo "### AHORA INSTALAMOS LOS COMPLEMENTOS Y LUEGO REINICAMOS ######"
echo "###############################################################"


yum install epel-release -y
yum install -y net-tools  wget mlocate openssh-clients pciutils bison bison-devel zlib gtk2 gettext cronie sudo zlib-devel ncurses ncurses-devel openssl openssl-devel gcc make krb5-devel newt newt-devel kernel-devel-`uname -r` kernel-headers gcc-c++ gnutls-devel curl curl-devel subversion nmap iftop htop wireshark screen lsof flex patch libtool* make gcc patch perl bison gcc-c++ ncurses-devel flex flex-devel libtermcap-devel autoconf automake autoconf audiofile-devel httpd sox  libxml2 libxml2-devel libtiff libtiff-devel lame openssl openssl-devel perl perl-CPAN 

yum install -y curl libuuid-devel unixODBC unixODBC-devel libtool-ltdl ncurses-devel uuid-devel libuuid-devel libxml2-devel sqlite-devel mariadb-devel mariadb-server bison subversion ntp cyrus-sasl sendmail git-core libtalloc libtdb samba-winbind-clients samba samba-client samba-common db4-devel pam-devel libedit-devel libtool-ltdl-devel libc-client-devel sqlite2-devel t1lib-devel libmcrypt-devel libtidy-devel freetds-devel aspell-devel recode-devel enchant-devel bzip2-devel httpd-devel mailx postfix cyrus-sasl-devel cyrus-sasl-plain openldap-devel postgresql-devel net-snmp-devel libxslt-devel libjpeg-devel libpng-devel freetype-devel libXpm-devel libicu-devel

yum group mark install "Development Tools" -y
#####yum group update "Development Tools" -y
yum install bind-utils -y
yum install neovim -y


ln -sf /lib64/libtinfo.so.5 /lib64/libtermcap.so.2
systemctl start mariadb 
systemctl enable mariadb

#echo "y vamos aceptando las condiciones que se vayan presentando durante la instalacion de las dependencias."

#reboot

#;;

#3) echo "INSTALACION DE DAHDI LINUX, DAHDA TOOLS, LIBPRI, MPG123, SPANDSP, PHP,
#         JANSSON, ASTERISK 13.21, SONIDOS "

echo "###############################################################"
echo "### PONEMOS TODA LA CARPETA ASTERISK EN LA RUTA /usr/src ######"
echo "###############################################################"

echo "#####################################"
echo "### instalacion de dahdi linux ######"
echo "#####################################"
mkdir -p /usr/src/asterisk
yum install wget -y
cd /usr/src/asterisk/
wget downloads.asterisk.org/pub/telephony/dahdi-linux/dahdi-linux-2.10.2.tar.gz

tar -xf dahdi-linux-2.10.2.tar.gz
cd /usr/src/asterisk/dahdi-linux*

make
make install
cd ..

echo "#####################################"
echo "### instalcion de dahadi tools ######"
echo "#####################################"

cd /usr/src/asterisk/
wget downloads.asterisk.org/pub/telephony/dahdi-tools/dahdi-tools-2.10.2.tar.gz
tar -xf dahdi-tools-2.10.2.tar.gz
cd /usr/src/asterisk/dahdi-tools*
autoreconf -f -i
./configure
make all
make install
make config
cd ..

echo "############################"
echo "### instalcion libpri ######"
echo "############################"
cd /usr/src/asterisk
wget downloads.asterisk.org/pub/telephony/libpri/libpri-1.6.0.tar.gz
tar -xf libpri-1.6.0.tar.gz
cd /usr/src/asterisk/libpri*
make all
make install
cd ..

echo "###############################"
echo "### instalcion de mpg123 ######"
echo "###############################"

cd /usr/src/asterisk
yum install mpg123 -y

#cd /usr/src/asterisk/mpg123*
#./configure
#make
#make install
#ln -s /usr/local/bin/mpg123 /usr/bin/mpg123
#cd ..
#
echo "################################"
echo "### instalcion de Spandsp ######"
echo "################################"

cd /usr/src/asterisk/
yum install spandsp -y

echo "################################"
echo "### instalcion de PHP ##########"
echo "################################"
mv backup.zip /usr/src/asterisk
cd /usr/src/asterisk
cd /usr/src/asterisk/backup/x86_64
rpm -ivh php-*.rpm
cd /usr/src/asterisk/backup
rpm -ivh php-pear-*.rpm
cd ..

echo "################################"
echo "### instalcion de Jansson ######"
echo "################################"

mv jansson.tar.xz /usr/src/asterisk
cd /usr/src/asterisk/
tar -xf jansson.tar.xz
cd /usr/src/asterisk/jasson
autoreconf -i
./configure -prefix=/usr/
make
make install
cd ..

echo "#################################"
echo "### instalcion de Asterisk ######"
echo "#################################"

cd /usr/src/asterisk/asterisk-13*
./bootstrap.sh && ./contrib/scripts/get_mp3_source.sh && make distclean
./configure
##cd configure
##make menuselect
make menuconfig
##cd ..
##make menuselect-tree
##menuselect/menuselect --enable format_mp3 --enable cdr_mysql --enable app_mysql --enable res_config_mysql --enable app_fax --enable app_meetme --enable cel_odbc --enable cdr_adaptive_odbc menuselect.makeopts
##
read -p "Presione la tecla [Enter] para continuar...  "

make
make install
make samples
make config
make install-logrotate

systemctl start asterisk
systemctl enable asterisk

cd /usr/src/asterisk/
cp -rf confbridge_additional.conf /etc/asterisk/
cp -rf confbridge_custom.conf /etc/asterisk/
cat confbridge.conf >> /etc/asterisk/confbridge.conf

asterisk -rx "module load app_confbridge.so"
asterisk -rx "reload"
cd ..

##echo "#################################"
##echo "### instalcion de Wanpipe #######"
##echo "#################################"

##cd /usr/src/asterisk/wanpipe*
##./Setup dahdi
##cd ..

echo "#####################################"
echo "### exportando  los sonidos  #######"
echo "#####################################"

cd /usr/src/asterisk
cp -r /usr/src/asterisk/es /var/lib/asterisk/sounds

echo "###########################"
echo "### dando permisos  #######"
echo "###########################"

adduser -c "Asterisk PBX" -d /var/lib/asterisk/  asterisk
chown -R asterisk:asterisk /etc/asterisk/
chown -R asterisk:asterisk /var/lib/asterisk/
chown -R asterisk:asterisk /var/spool/asterisk/
chown -R asterisk:asterisk /var/log/asterisk/
chown -R asterisk:asterisk /usr/lib/asterisk/
chown -R asterisk:asterisk /var/run/asterisk/
chown -R asterisk:asterisk /var/lib/php/

echo "###############################################"
echo "### MEJORANDO LIMITE DE MEMORIA DE PHP  #######"
echo "###############################################"

sed -i "464s/memory_limit = 128M/memory_limit = 300M/" /etc/php.ini
sed -i "885s/upload_max_filesize = 2M/upload_max_filesize = 128M/" /etc/php.ini
echo  "LimitRequestBody 200000000" >> /etc/httpd/conf.d/php.conf
rm -rf /etc/localtime
ln -sf /usr/share/zoneinfo/America/Lima /etc/localtime
sed -i "379s|^|date.timezone ='America/Lima'|" /etc/php.ini
sed -i "72s/;runuser = asterisk/runuser = asterisk/" /etc/asterisk/asterisk.conf
sed -i "73s/;rungroup = asterisk/rungroup = asterisk/" /etc/asterisk/asterisk.conf
sed -i "8s/create 640 root root/create 640 asterisk asterisk/" /etc/logrotate.d/asterisk
sed -i '8s/#AST_USER="asterisk"/AST_USER="asterisk"/g' /etc/sysconfig/asterisk
sed -i '9s/#AST_GROUP="asterisk"/AST_GROUP="asterisk"/g'  /etc/sysconfig/asterisk 

echo "--------------------------------------------------------------"
echo "################################"
echo "################################"
echo "### CONFIGURANDO MARIADB  ######"
echo "### CONFIGURANDO MARIADB  ######"
echo "################################"
echo "################################"
sleep 3

read -p "Presione la tecla [Enter] para continuar..."
systemctl start mariadb
##mysql_secure_installation
#####################################################################
####debajo de la primera linea se deja vacia como si fuece un enter
#mysql_secure_installation << 'EOF'
#sesamo
#y
#parque
#parque
#y
#y
#y
#y
#EOF
#
usermaria="parque"
userdb="parquedb"
userpassdb="parquepass"

mysql -u root -p${usermaria} -e "CREATE DATABASE asterisk; CREATE DATABASE asteriskcdrdb; GRANT ALL PRIVILEGES ON asterisk.* TO '$userdb'@'localhost' IDENTIFIED BY '$userpassdb'; GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO '$userdb'@'localhost' IDENTIFIED BY '$userpassdb'; FLUSH PRIVILEGES; "
echo "----------------------------------------"
echo "HOLA HOY ES TU DIA DE SUERTE, fueron creadas las bases de datos asterisk y asteriskcdrb"

####################################################################
#sleep 3
#clear
#
#echo "lea con atención y no se apresure , escriba las contraseñas que uested creara "
#read -p "Presione la tecla [Enter] para continuar.... "
#echo "ESCRIBA LA MISMA CONTRASEÑA QUE USO PARA MARIADB:"
#read DATABASE_PASS
#echo "CONTRASEÑA DE MARIADB: $DATABASE_PASS" >> /usr/src/asterisk/variables.txt
#
#echo "ESCRIBA UN NOMBRE PARA CREAR EL USUARIO, ESTE USUARIO ADMINISTRARA LAS BASES DE DATOS  ASTERISK Y ASTERISKCDRDB"
#read USER_DB
#echo "ESCRIBIR UN NOMBRE DE  USUSARIO: $USER_DB" >> /usr/src/asterisk/variables.txt
#
#echo "ESCRIBA UNA CONTRASEÑA PARA EL USUARIO CREADO, QUIEN ADMINISTRARA LAS DOS BASES DE DATOS  ASTERISK y ASTERISKCDRDB"
#read USER_PASS_DB
#echo "CONTRASEÑA DEL USUARIO: $USER_PASS_DB" >> /usr/src/asterisk/variables.txt
#
#clear
#echo "LAS CONTRASEÑAS FUERON GUARDADAS EN /USR/SRC/ASTERISK/VARIABLES.TXT"
#read -p "Presione la tecla [Enter] para continuar..."
#
#
#
#echo " ###############################################"
#echo "               CREANDO BD                       "
#echo " ###############################################"
#echo ""
#sleep 3
#read -p "Presione la tecla [Enter] para continuar..."
#mysql -u root -p${DATABASE_PASS} -e "CREATE DATABASE asterisk; CREATE DATABASE asteriskcdrdb; GRANT ALL PRIVILEGES ON asterisk.* TO '$USER_DB'@'localhost' IDENTIFIED BY '$USER_PASS_DB'; GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO '$USER_DB'@'localhost' IDENTIFIED BY '$USER_PASS_DB'; FLUSH PRIVILEGES; "
#echo "----------------------------------------"
#echo "HOLA HOY ES TU DIA DE SUERTE, fueron creadas las bases de datos asterisk y asteriskcdrb"
#echo "----------------------------------------"
#
#
#sleep 3
#
echo " ###############################################"
echo " ###############################################"
echo " ###############################################"
echo "            INSTALANDO FREEPBX                  "
echo " ###############################################"
echo ""
read -p "Presione la tecla [Enter] para continuar..."
cd /usr/src/asterisk/freepbx*/SQL
echo " ESCRIBA LA MISMA CONTRASEÑA QUE USO PARA ENTRAR A MARIADB "
mysql asteriskcdrdb -p"$usermaria" < ./cdr_mysql_table.sql
echo " VUELVA A ESCIRBIR LA MISMA CONTRASEÑA DE MAIRADB "
mysql asterisk -p"$usermaria" < ./newinstall.sql
echo "creando enlace simbolico "
ln -sf /usr/share/zoneinfo/America/Lima /etc/localtime


sed -i "66s/# User apache/User asterisk/" /etc/httpd/conf/httpd.conf
sed -i "67s/# Group apache/Group asterisk/" /etc/httpd/conf/httpd.conf

sed -i "66s/User apache/User asterisk/" /etc/httpd/conf/httpd.conf
sed -i "67s/Group apache/Group asterisk/" /etc/httpd/conf/httpd.conf
              
sed -i "42s/Listen 80/Listen 3030/" /etc/httpd/conf/httpd.conf   
sed -i ‘95s/# ServerName www.example.com:80/  ServerName $(hostname):3030/g’ /etc/httpd/conf/httpd.conf

 sed -i "1s/127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4/127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 $(hostname)/" /etc/hosts


systemctl enable asterisk
systemctl enable mariadb
systemctl enable httpd
systemctl start asterisk
systemctl start mariadb
systemctl restart httpd

cd /usr/src/asterisk/freepbx*
clear
##read -p "Presione la tecla [Enter] para continuar..."
echo "Iniciando asterisk "
./start_asterisk start
####read -p "Presione la tecla [Enter] para continuar..."

sleep 4

echo " RECUERDE QUE EN LAS DOS PRIMERAS PREGUNTAS USTEDE DEBE PONER EL USUARIO Y PASSWORD CREADO EN MARIADB, ESTE USUARIO ES EL QUE ADMINISTRARA LAS DOS BASES DE DATOS LLAMADAS: ASTERISK Y ASTERISKCDRDB "

./install_amp << 'EOF'
parquedb
parquepass

parqueami
parquepassami

192.168.1.92
parquepassfop



EOF

sleep 4

read -p "Presione la tecla [Enter] para continuar..."

asterisk -rx 'module reload cdr_mysql.so'

cd /etc/asterisk
rm -rf sip_notify.conf
ln -sf  /var/www/html/admin/modules/core/etc/sip_notify.conf /etc/asterisk/sip_notify.conf
sed -i "10s/CONSOLE=yes/CONSOLE=no/" /usr/sbin/safe_asterisk
echo "/usr/local/sbin/amportal start" >> /etc/rc.local
chmod 777 /etc/rc.local
cd /var/www/html
chown -R asterisk:asterisk *

amportal start

echo -e  "asterisk ALL = NOPASSWD: /bin/bash\nasterisk ALL = NOPASSWD: /bin/chmod\nasterisk ALL = NOPASSWD: /bin/chown\nasterisk ALL = NOPASSWD: /sbin/service\nasterisk ALL = NOPASSWD: /etc/init.d/network\nasterisk ALL = NOPASSWD: /sbin/reboot\nasterisk ALL = NOPASSWD: /sbin/halt\nasterisk ALL = NOPASSWD: /bin/mv\nasterisk ALL = NOPASSWD: /bin/cp\nasterisk ALL = NOPASSWD: /bin/mkdir\nasterisk ALL = NOPASSWD: /bin/touch\nasterisk ALL = NOPASSWD: /usr/local/sbin/amportal\nasterisk ALL = NOPASSWD: /bin/rm\nasterisk ALL = NOPASSWD: /bin/sh\nasterisk ALL = NOPASSWD: /bin/tar" >> /etc/sudoers


sleep 4
echo "AHORA EN EL NAVEGADOR PONER LA IP-DEL-SERVER:3030 Y ACCEDES PONIENDO EN USER
      Y PASSWORD: admin admin, e instalas los modulos necesarios para el ePBX y no
      olvidar que solo debe estar el usuario root y de password admin  "


echo "language=es" >> /etc/asterisk/sip_general_custom.conf
echo "videosupport=yes">> /etc/asterisk/sip_general_custom.conf
echo "allow=h264">> /etc/asterisk/sip_general_custom.conf
echo "allow=263">> /etc/asterisk/sip_general_custom.conf
echo "progressinband=yes">>/etc/asterisk/sip_general_custom.conf
echo "prematuremedia=no">> /etc/asterisk/sip_general_custom.conf

sed -i  "45s/console => notice,warning,error/console => notice,warning,error,debug,verbose,dtmf,fax/"  /etc/asterisk/logger_logfiles_custom.conf

sed -i "47s/messages => notice,warning,error/messages => notice,warning,error,debug,verbose,dtmf,fax/" /etc/asterisk/logger_logfiles_custom.conf

echo "full => notice,warning,error,debug,verbose,dtmf,fax">> /etc/asterisk/logger_logfiles_custom.conf



;;

0) echo "Gracias ahora continua con el script: rapidito2.sh.
         VAYA AL NAVEGADOR E INSTALE LOS MODULOS EN ESTA PARTE
         DEL FREEPBX y RECUERDE QUE SOLO DEBE ESTAR EL USUARIO
         ROOT y de password ADMIN" ; exit 0

esac
done



