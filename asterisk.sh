#!/bin/bash

yum install -y sed # instala sed - stream editor
##sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/sysconfig/selinux #esto es un ubuntu server
sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/selinux/config #esto es en centos7

cd /usr/src
yum install epel-release -y
yum install wget 
wget downloads.asterisk.org/pub/telephony/asterisk/asterisk-13-current.tar.gz
wget downloads.asterisk.org/pub/telephony/dahdi-linux/dahdi-linux-current.tar.gz
wget downloads.asterisk.org/pub/telephony/dahdi-tools/dahdi-tools-3.1.0.tar.gz
wget downloads.asterisk.org/pub/telephony/libpri/libpri-current.tar.gz
wget downloads.asterisk.org/pub/telephony/sounds/asterisk-core-sounds-es-gsm-current.tar.gz

tar -xf asterisk-13-current.tar.gz
tar -xf dahdi-linux-current.tar.gz
tar -xf dahdi-tools-3.1.0.tar.gz
tar -xf libpri-current.tar.gz

## Instalar utilidades opcionales
 yum -y install vim sed awk wget # editor de tecxto avanzado
 
## Herramientas de compilación
 yum install -y gcc  # compilador gnu c
 yum install -y gcc-c++ # compilador gnu c++
 yum install -y kernel-devel # código fuente del kernel de asterisk
 yum install -y automake # herramienta de compilación que genera archivo Makefile
## Bibliotecas de desarrollo
 yum install -y ncurses-devel # bibliotecas de desarollo con ncurses (interfaz de texto)
 yum install -y newt-devel #
 yum install -y libuuid-devel # biblioteca para generar ids únicos
 yum install -y jansson-devel # biblioteca para manipular strings json
 yum install -y libxml2-devel # biblioteca para manipular strinds xml
 yum install -y sqlite-devel  # biblioteca para conectarte con base de datos sqlite
 yum install -y install libtool-ltdl libtool-ltdl-devel 

##esta parate es un resumen de las dependecias para instalar asterisk13
yum install epel-release -y
yum install vim sed gawk wget gcc gcc-c++ kernel-devel automake ncurses-devel newt-devel libuuid-devel jansson-devel libxml2-devel sqlite-devel libtool-ltdl libtool-ltdl-devel autoconf automake m4 perl && yum groupinstall 'Development Tools' -y
yum update -y
#reboot 
#=====================================
cd /usr/src/dahdi-linux-3.*/
make
make install
#=====================================
cd /usr/src/dahdi-tools-3.*/
##si no encontramos el configure, correr el siguiente comando para crear el configure.
autoreconf -f -i
./configure
make all      # compila todos los binarios relacionados con DAHDI
make install  # instala binarios de DAHDI y DAHDI tools
make config   # configura??? -&gt; scripts de control?
#=====================================
cd /usr/src/libpri-1.*/
make all
make install
#=====================================
cd /usr/src/asterisk-13.*/
./configure
make all
make install
make samples
make config
#=====================================
##sonido en espaniol
mv /var/lib/asterisk/sounds/en /var/lib/asterisk/sounds/en-fue
mkdir -p /var/lib/asterisk/sounds/en
cd /usr/src/
mv asterisk-core-sounds-es-gsm-current.tar.gz /var/lib/asterisk/sounds/en
cd /var/lib/asterisk/sounds/en && tar -xf *
##asterisk -rvvvvvvvvvvv y nos dá error debemos reiniciar el server.
systemctl stop  asterisk
systemctl start asterisk
systemctl enable asterisk

